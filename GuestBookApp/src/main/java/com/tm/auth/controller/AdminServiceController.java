package com.tm.auth.controller;

import com.tm.auth.model.User;
import com.tm.auth.service.SecurityService;
import com.tm.auth.service.UserService;
import com.tm.auth.validator.UserValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/GuestApp/Admin/")
@Controller
public class AdminServiceController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator; 
    
    @PostMapping("adminlogin")
    public String adminlogin(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "adminlogin";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }
 
    @GetMapping({"/welcome"})
    public String welcomeUser(Model model) {
        return "homePageAdmin";
    } 
}
