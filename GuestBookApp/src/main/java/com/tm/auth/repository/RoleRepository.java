package com.tm.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tm.auth.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
