package com.tm.auth.service;

import com.tm.auth.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
